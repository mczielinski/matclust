function out = findNoiseEvents()

global clustattrib

percentChannels = .3;
window = .001;

currentParamFile = clustattrib.currentparamfilename;
cd(clustattrib.currentfilepath);
underscores = strfind(currentParamFile,'_');
channel = str2num(currentParamFile(underscores(1)+1:underscores(2)-1));

files = dir('*_SpikeParams.mat');
spikeTimes = [];

for i = 1:length(files)
    underscores = strfind(files(i).name,'_');
    channelNum = str2num(files(i).name(underscores(1)+1:underscores(2)-1));
    load(files(i).name);
    spikeTimes(channelNum).t = filedata.params(:,1);   
end

numChannels = 0;
coincideCount = zeros(size(spikeTimes(channel).t));
channelTimes = spikeTimes(channel).t;
for i = 1:length(spikeTimes)
    if (i ~= channel)
        tmpind = lookup(channelTimes,spikeTimes(i).t);
        if ~isempty(tmpind)
            numChannels = numChannels+1;
            inWindow = abs(channelTimes-spikeTimes(i).t(tmpind)) < window;
            coincideCount = coincideCount+inWindow;
        end
    end
end

out = ((coincideCount/numChannels) < percentChannels);